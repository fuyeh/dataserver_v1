"""Defines the apk module."""
import os
import shutil
from config import UPLOAD_FOLDER_apk
from werkzeug.utils import secure_filename
from flask import render_template, request, Blueprint, flash, redirect, url_for

bp = Blueprint('apk', __name__, url_prefix='/apk')

# 只允許上傳apk檔
ALLOWED_EXTENSIONS = {'apk'}


def allowed_file(filename):
    return '.' in filename and filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


def vender_exist(vender):
    vender_path = os.path.join(UPLOAD_FOLDER_apk, vender)
    vender_exist = os.path.isdir(vender_path)             # 檢查是否存在相對應subversion資料夾
    if not vender_exist:
        os.mkdir(vender_path)                             # 創造相對應vender資料夾


def delete_apk(vender, version):
    apkv_path = os.path.join(UPLOAD_FOLDER_apk, vender, version)
    apkv_exist = os.path.isfile(apkv_path)                        # 檢查檔案是否存在
    if apkv_exist:
        os.remove(apkv_path)                                      # 刪除檔案
        vender_path = os.path.join(UPLOAD_FOLDER_apk, vender)
        if not os.listdir(vender_path):                           # 如果資料夾是空的
            shutil.rmtree(vender_path)                            # 刪除資料夾


@bp.route('/upload', methods=['GET', 'POST'])
def upload():
    if request.method == 'POST':
        filename = request.files['file'].filename
        filenamelist = filename.split("-")
        vender = filenamelist[1]
        # check vender directory exist and create new directory
        vender_exist(vender)

        # check if the post request has the file part
        if 'file' not in request.files:
            flash('No file part')
            return redirect(request.url)
        file = request.files['file']
        # If the user does not select a file, the browser submits an empty file without a filename.
        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            UPLOAD_FOLDER_ = os.path.join(UPLOAD_FOLDER_apk, vender, filename)
            file.save(UPLOAD_FOLDER_)
            return redirect(url_for('apk.upload'))
    return render_template('pages/apk_upload.html')


@bp.route('/delete', methods=["GET", "POST"])
def delete():
    if request.method == 'POST':
        # APK filename
        filename = request.form['filename']
        filenamelist = filename.split('-')
        vender = filenamelist[1]
        # check apk exist and delete
        delete_apk(vender, filename)
    return render_template('pages/apk_delete.html')

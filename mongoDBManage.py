"""Defines to search or store data in mongoDB module."""
import os
import time
import shutil
import pymongo
import pandas as pd
from datetime import datetime
from bson.objectid import ObjectId
from config import Algopath, BCGpath
from config import MONGO_HOST_IP, MONGO_NAME, MONGO_PWD

myclient = pymongo.MongoClient(MONGO_HOST_IP)
myclient.admin.authenticate(MONGO_NAME, MONGO_PWD)
mongo_db = myclient["BiologueDB"]   # create my database


def find_one_and_update(email, pwd_hash):
    result = mongo_db["Dataserver_Users"].find_one_and_update(filter={"email": email},
                                                   update={"$set": {'password': pwd_hash}})
    return result


def registerUserIDToMongoDB(email, username, password):
    timestamp = time.time()
    col_ref = mongo_db["Dataserver_Users"]
    if col_ref.count_documents({"email": email}) > 0:
        # exit the email
        return False
    insert_one = col_ref.insert_one(
        {"username": username,
         "email": email,
         "password": password,
         "timestamp": timestamp}
    )
    return insert_one.inserted_id


def registerDataToMongoDB(col_name, data):
    col_ref = mongo_db[col_name]
    insert_one = col_ref.insert_one(data)
    return insert_one.inserted_id


# print all daatabase
print(myclient.list_database_names())


def queryNewestMongoDB(col_name, limit, timestamp):
    col_ref = mongo_db[col_name].find({
        "timestamp": {
            "$lte": timestamp
        }}).sort("timestamp", -1).limit(limit)

    tempList = []
    for col in col_ref:
        tempList.append(col)
    return tempList


def queryMongoDB(col_name, query_dict):
    col_ref = mongo_db[col_name].find(query_dict)
    tempList = []
    for d in col_ref:
        tempList.append(d)
    return tempList


def saveUserIDToMongoDB(col_name, email, username, password):
    timestamp = time.time()
    col_ref = mongo_db[col_name]

    col_ref.delete_one({"email": email})  # 刪除舊資料

    insert_one = col_ref.insert_one(
        {
            "email": email,
            "username": username,
            "password": password,
            "timestamp": timestamp
        }
    )
    return insert_one.inserted_id


def emailToUserid(email):
    userid = list(mongo_db["Users"].find({"email": email}, {"_id": 1}))
    if userid:    # check email to userid exist
        return userid[0]["_id"]
    return False


def useridToEmail(userid):
    email = list(mongo_db["Users"].find({"_id": userid}, {"email": 1}))
    if email:   # check email to email exist
        return email[0]["email"]
    return False


def clean(path):
    shutil.rmtree(path)
    os.mkdir(path)     # 創資料夾


def findBCGByUserid(userid, Stimestamp, Etimestamp):
    col_ref = mongo_db["BCG"].find({
        "userid": userid,
        # "carid": carid,
        "timestamp": {
            "$gte": Stimestamp * 1000,
            "$lte": Etimestamp * 1000
        }},
        {'_id': 0,
         'bcg': 1,
         'accx': 1,
         'accy': 1,
         'accz': 1,
         'timestamp': 1,
         'algoidlist': 1
         }
    ).sort("timestamp", 1)

    start_timestamp = mongo_db["BCG"].find({
        "userid": userid,
        # "carid": carid,
        "timestamp": {
            "$gte": Stimestamp * 1000,
            "$lte": Etimestamp * 1000
        }},
        {'_id': 0,
         'timestamp': 1
         }
    ).sort("timestamp", 1).limit(1)

    return col_ref, start_timestamp


def findUserids(Stimestamp, Etimestamp):
    userids = mongo_db["BCG"].aggregate([
        {"$match": {"timestamp": {"$gte": Stimestamp * 1000, "$lte": Etimestamp * 1000}}},
        {"$project": {"_id": 0, "userid": 1}},
        {"$group": {"_id": "$userid"}}
    ])

    return list(userids)


# 再將BCG資料依照email儲存
def SaveBCGData(col_ref, start_timestamp, email, fileformat):
    start_timestampp = list(start_timestamp)   # 如果沒資料就直接回傳False
    if not start_timestampp:
        return False
    start_timestampp = start_timestampp[0]['timestamp']
    start_time = datetime.fromtimestamp(start_timestampp*0.001).strftime('%Y/%m/%d-%H:%M:%S:%f')
    filename_start_time = datetime.fromtimestamp(start_timestampp * 0.001).strftime('%Y-%m-%d %H-%M-%S')
    # 先創空list
    bcg, accx, accy, accz, hr, resp, status = [list() for i in range(7)]

    for col in col_ref:
        hz = int(len(col['bcg']) / 60)                      # hz = bcg長度/60
        current_timestamp = col['timestamp']
        col.pop("timestamp")

        algoid = col['algoidlist'][0]                       # 目前一筆algo對一筆bcg
        if len(algoid) != 0:
            algodata = findExpandAlgoData(algoid, hz)       # 擴張的 algo data(64hz)
            diff = current_timestamp-start_timestampp       # 時間差

            if diff < 300000:      # 五分鐘 = 300000
                bcg.extend(col['bcg'][:60 * hz])    # 因為hr可能超過60*hz筆所以限制60*hz筆
                accx.extend(col['accx'][:60 * hz])  # 因為hr可能超過60*hz筆所以限制60*hz筆
                accy.extend(col['accy'][:60 * hz])  # 因為hr可能超過60*hz筆所以限制60*hz筆
                accz.extend(col['accz'][:60 * hz])  # 因為hr可能超過60*hz筆所以限制60*hz筆
                hr.extend(algodata['hr'])
                resp.extend(algodata['resp'])
                status.extend(algodata['status'])
                start_timestampp = current_timestamp

            else:
                # 先輸出連續部分df
                temp_dict = {'bcg': bcg, 'accx': accx, 'accy': accy, 'accz': accz, 'hr': hr, 'resp': resp, 'status': status}
                df = pd.DataFrame(temp_dict)

                df.insert(0, 'zero', 0)
                df.columns = ['Start Time: ' + str(start_time), '', '', '', '', '', '', '']
                # 輸出路徑
                filepath = os.path.join(BCGpath, email, "{}_bcg.{}".format(str(filename_start_time), fileformat))
                df.to_csv(filepath, index=False)
                # 校正回歸
                bcg, accx, accy, accz, hr, resp, status = [list() for i in range(7)]    # 先創空list
                bcg.extend(col['bcg'][:60 * hz])    # 因為hr可能超過60*hz筆所以限制60*hz筆
                accx.extend(col['accx'][:60 * hz])  # 因為hr可能超過60*hz筆所以限制60*hz筆
                accy.extend(col['accy'][:60 * hz])  # 因為hr可能超過60*hz筆所以限制60*hz筆
                accz.extend(col['accz'][:60 * hz])  # 因為hr可能超過60*hz筆所以限制60*hz筆
                hr.extend(algodata['hr'])
                resp.extend(algodata['resp'])
                status.extend(algodata['status'])
                start_timestampp = current_timestamp
                start_time = datetime.fromtimestamp(current_timestamp*0.001).strftime('%Y/%m/%d-%H:%M:%S:%f')
                filename_start_time = datetime.fromtimestamp(current_timestamp * 0.001).strftime('%Y-%m-%d %H-%M-%S')

    # 輸出最後一筆資料
    temp_dict = {'bcg': bcg, 'accx': accx, 'accy': accy, 'accz': accz, 'hr': hr, 'resp': resp, 'status': status}
    df = pd.DataFrame(temp_dict)
    df.insert(0, 'zero', 0)
    df.columns = ['Start Time: ' + str(start_time), '', '', '', '', '', '', '']
    # 輸出路徑
    filepath = os.path.join(BCGpath, email, "{}_bcg.{}".format(str(filename_start_time), fileformat))
    df.to_csv(filepath, index=False)

    return True   # 資料成功儲存就回傳True


def zip(Dpath):
    zipfiledir = shutil.make_archive('data', 'zip', Dpath)
    return zipfiledir


def sortDirectorys(directorys):
    dss = [d.split('.')[0].split('_') for d in directorys]
    s = sorted(dss, key=lambda x: (int(x[0]), int(x[1]), int(x[2])), reverse=True)
    sorted_directorys = [i[0] + '_' + i[1] + "_" + i[2] + '.bin' for i in s]
    return sorted_directorys


def timestampToDatetime(timestamp):
    return datetime.fromtimestamp(timestamp*0.001).strftime('%Y/%m/%d-%H:%M:%S:%f')


def findAlgoByUserid(userid, Stimestamp, Etimestamp):
    col_ref = mongo_db["Algo"].find({
        "userid": userid,
        # "carid": carid,
        "timestamp": {
            "$gte": Stimestamp * 1000,
            "$lte": Etimestamp * 1000
        }}

    ).sort("timestamp", 1)
    return col_ref


# 再將Algo資料依照email儲存
def SaveAlgoData(col_ref, email, fileformat):
    col_ref = list(col_ref)  # 如果沒資料就直接回傳False
    if not col_ref:
        return False
    df = pd.DataFrame(col_ref)
    # 將timestamp轉成datetime
    df['datetime'] = df['timestamp'].apply(timestampToDatetime)
    # 輸出路徑
    filepath = os.path.join(Algopath, email, "algo_{}.{}".format(email, fileformat))
    df.to_csv(filepath, index=False)
    return True


def findExpandAlgoData(algoid, hz):
    algodata = mongo_db["Algo"].find({
        "_id": ObjectId(algoid)
    },
        {'_id': 0,
         'hr': 1,
         'resp': 1,
         'status': 1
         }
    )

    algodata = list(algodata)[0]
    algodata['hr'] = [h for h in algodata['hr'][:60] for i in range(hz)]          # 因為hr可能超過60筆所以限制60筆，因為hz所以重複hz次
    algodata['resp'] = [r for r in algodata['resp'][:60] for i in range(hz)]      # 因為resp可能超過60筆所以限制60筆，因為hz所以重複hz次
    algodata['status'] = [s for s in algodata['status'][:60] for i in range(hz)]  # 因為status可能超過60筆所以限制60筆，因為hz所以重複hz次

    return algodata
"""Define non-public information"""
import os

# Grabs the folder where the script runs.
basedir = os.path.abspath(os.path.dirname(__file__))

# Enable debug mode.
DEBUG = True

# Secret key for session management. You can generate random strings here:
# https://randomkeygen.com/
SECRET_KEY = 'uC*bD!tE5q|VR20o4.K&'

MONGO_HOST_IP = "mongodb://192.168.0.86:27017/"    # 記得server 127.0.0.1:27017
MONGO_NAME = "biologue"
MONGO_PWD = "server6688436"

# homedir = os.path.expanduser('~')
# folder = '/web/apk_dir'
# UPLOAD_FOLDER_apk = homedir + folder             # apk_dir絕對路徑
UPLOAD_FOLDER_apk = 'D:/apk_dir'

# homedir = os.path.expanduser('~')
# folder = '/web/firmware_dir'
# UPLOAD_FOLDER_firmware = homedir + folder       # firmware_dir絕對路徑
UPLOAD_FOLDER_firmware = 'D:/firmware_dir'


# homedir = os.path.expanduser('~')
# datafiledir = '/web/fuyeh_env/dataserver_v1/data'
# Algofiledir = '/web/fuyeh_env/dataserver_v1/data/Algo'
# BCGfiledir = '/web/fuyeh_env/dataserver_v1/data/BCG'
# datapath = homedir + datafiledir
# Algopath = homedir + Algofiledir
# BCGpath = homedir + BCGfiledir
datapath = 'D:\\dataserver_v1\\data'
Algopath = 'D:\\dataserver_v1\\data\\Algo'
BCGpath = 'D:\\dataserver_v1\\data\\BCG'
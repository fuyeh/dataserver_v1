"""Defines the auth module."""

import re
import bcrypt
from forms import RegisterForm, LoginForm, ForgotForm, ProfileForm
from flask import render_template, request, Blueprint
from flask import redirect, url_for, flash, session, current_app
from mongoDBManage import registerUserIDToMongoDB, queryMongoDB, saveUserIDToMongoDB, find_one_and_update

bp = Blueprint("auth", __name__, url_prefix="/auth")


def verifyEmail(email):
    pattern = re.compile(r"[^@]+@[^@]+\.[^@]+")
    if re.match(pattern, email) is not None:
        return True
    else:
        return False


@bp.route('/login', methods=["GET", "POST"])
def login():
    form = LoginForm(request.form)
    if request.method == "POST":
        if request.values["login"] == "Login":
            # Get the users from mongoDB
            query = {
                "email": {
                    "$regex": request.form["email"]
                },
                "username": {
                    "$regex": request.form["username"]
                }
            }
            result = queryMongoDB("Dataserver_Users", query)

            if len(result) > 0:
                for r in result:
                    # now check the password
                    pwd_hash = r["password"]
                    # try to convert the type to byte array
                    try:
                        pwd_hash = pwd_hash.encode()

                    except:
                        pass

                    global userid, carid
                    userid = r["_id"]

                    if bcrypt.hashpw(str.encode(request.form["password"]), pwd_hash) == pwd_hash:
                        current_app.logger.debug("password match")

                        # 這邊定義seesion #
                        session.clear()
                        session["username"] = request.form["username"]
                        session["email"] = request.form["email"]

                        return redirect(url_for("home"))
                    else:
                        current_app.logger.debug("password error")
                        flash("Password error")
            else:
                flash("No email or username found")

    return render_template('forms/login.html', form=form)


@bp.route('/register', methods=["GET", "POST"])
def register():
    current_app.logger.debug("register")
    form = RegisterForm(request.form)

    try:
        if request.method == "POST":
            if request.values["register"] == "Register":
                if not verifyEmail(request.form["email"]):
                    flash("Email format is not valid")
                    return render_template('forms/register.html', form=form)

                # 再次確認密碼
                if request.form["password"] == request.form["confirm"]:
                    pwd_hash = bcrypt.hashpw(str.encode(request.form["password"]), bcrypt.gensalt())

                    #= check email and register the User information to MongoDB =#
                    userid = registerUserIDToMongoDB(
                        email=form.email.data,
                        username=form.username.data,
                        password=pwd_hash
                    )

                    if userid != False:
                        return redirect(url_for("auth.login"))
                    else:
                        flash("Email exist")
                else:
                    current_app.logger.debug("password not match")
                    flash("Confirm password not match")
    except Exception as e:
        flash("Register fail !!!")
        current_app.logger.debug(e)
    return render_template('forms/register.html', form=form)


@bp.route("/logout")
def logout():
    session.clear()
    return redirect(url_for("home"))


@bp.route('/forgot', methods=["GET", "POST"])
def forgot():
    form = ForgotForm(request.form)
    if request.method == "POST":
        if request.values["save"] == "save":
            email = form.email.data
            pwd_hash = bcrypt.hashpw(str.encode(form.newpassword.data), bcrypt.gensalt())
            result = find_one_and_update(email, pwd_hash)
            if not result:
                flash("Email doesn't exist, cannot update")

    return render_template('forms/forgot.html', form=form)


@bp.route('/profile', methods=["GET", "POST"])
def profile():
    current_app.logger.debug("profile")
    form = ProfileForm(request.form)

    # TODO: 先去資料庫找該user 資訊填入
    username = session.get("username")
    email = session.get("email")
    query = {"username": {"$regex": username}, "email": {"$regex": email}}
    user_data = queryMongoDB(col_name="Dataserver_Users", query_dict=query)[0]

    form.username.data = username
    form.email.data = email

    try:
        if request.method == "POST":
            if request.values["save"] == "save":
                if not verifyEmail(request.values["email"]):
                    flash("Email format is not valid")
                    return render_template('forms/profile.html', form=form)

                # TODO:再次確認密碼
                pwd_hash = user_data["password"]

                # = check email and register the User information to MongoDB =## 判斷EMAIL是否有使用過，沒有的話，存該筆資料進去
                userid = saveUserIDToMongoDB(
                    col_name='Dataserver_Users',
                    username=username,
                    email=email,
                    password=pwd_hash,
                    )
                if userid != False:
                    return redirect(url_for("auth.profile"))
                else:
                    flash("Email exist")
    except Exception as e:
        flash("Save fail !!!")
        current_app.logger.debug(e)

    return render_template('forms/profile.html', form=form)

// line chart data
var lineData = {
  labels : [

  ],
  
  datasets : [{
    label: "HR",
    yAxisID: "HR",
    lineTension: 0,
    borderColor: 'rgb(255, 99, 132)',
    data : [
    ]
  },{label: "Status",
    yAxisID: "Status",
    lineTension: 0,
    borderColor: 'rgb(0, 218, 72)',
    data : [
    ]}
  ]
}

var mychart = document.getElementById("chart_ecg").getContext("2d");
var LineChartDemo = new Chart(mychart, {
  type: 'line',
  data: lineData,
  options: {
    scales: {
      yAxes: [
        {
          id: 'HR',
          type: 'linear',
          position: 'left',
          scaleLabel: {
            display: true,
            labelString: "HR"
          },
        },
        {
          id: 'Status',
          type: 'linear',
          position: 'right',
          scaleLabel: {
            display: true,
            labelString: "Status"
          },
          ticks: {
            min: 0,
            max: 5,
            stepSize: 1
          }
        }
      ]
    }
  }
});
LineChartDemo.update()

function updateHR(heart_rate) {
  var obj = JSON.parse(heart_rate)
  lb = obj.labels
  vl = obj.values_hr
  
  LineChartDemo.data.labels = lb
  LineChartDemo.data.datasets[0].data = vl
  LineChartDemo.update()
}

function updateStatus(status) {
  var obj = JSON.parse(status)
  lb = obj.labels
  vl = obj.values_status
  
  LineChartDemo.data.labels = lb
  LineChartDemo.data.datasets[1].data = vl
  LineChartDemo.update()
}

"""Defines the firmware module."""
import os
import json
import shutil
import pandas as pd
from datetime import datetime
from bs4 import BeautifulSoup
from config import UPLOAD_FOLDER_firmware
from werkzeug.utils import secure_filename
from flask import render_template, request, Blueprint, flash, redirect, url_for, Response

bp = Blueprint('firmware', __name__, url_prefix='/firmware')

ALLOWED_EXTENSIONS = {'bin'}


def allowed_file(filename):
    return '.' in filename and filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


def hardware_directory_exist(hv):
    path = os.path.join(UPLOAD_FOLDER_firmware, hv)
    exist = os.path.isdir(path)  # 檢查是否存在相對應硬體版本目錄
    if not exist:
        os.mkdir(path)           # 創造相對應硬體版本目錄


def delete_firmware(hv, fv):
    firmware_path = os.path.join(UPLOAD_FOLDER_firmware, hv, fv)
    firmware_exist = os.path.isfile(firmware_path)          # 檢查檔案是否存在
    if firmware_exist:
        os.remove(firmware_path)                            # 刪除檔案
        hv_path = os.path.join(UPLOAD_FOLDER_firmware, hv)  # 如果資料夾是空的
        if not os.listdir(hv_path):
            shutil.rmtree(hv_path)                          # 刪除資料夾


@bp.route('/upload', methods=['GET', 'POST'])
def upload():
    if request.method == 'POST':
        # hardware version
        hv = request.form['hv']
        # check hardware_version directory exist and create new directory
        hardware_directory_exist(hv)

        # check if the post request has the file part
        if 'file' not in request.files:
            flash('No file part')
            return redirect(request.url)
        file = request.files['file']
        # If the user does not select a file, the browser submits an empty file without a filename.
        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            fv = '_'.join(filename.split('.')[0].split('_')[0:3])
            UPLOAD_FOLDER_ = os.path.join(UPLOAD_FOLDER_firmware, hv, fv+'.bin')
            # 儲存"檔案"
            file.save(UPLOAD_FOLDER_)
            # 儲存firmware"資料"
            label = filename.split('.')[0].split('_')[3]

            d = [{'datetime': datetime.now().strftime("%Y/%m/%d %H:%M:%S"),
                  'hv': hv,
                  'fv': fv,
                  'label': label,
                  'verify': 'Not Pass',
                  'commit': None,
                  }
                 ]
            pd.DataFrame(d).to_csv(UPLOAD_FOLDER_firmware + '/FirmwareData.csv', mode='a', index=False, header=False)
            return redirect(url_for('firmware.upload'))
    return render_template('pages/firmware_upload.html')


@bp.route('/delete', methods=["GET", "POST"])
def delete():
    if request.method == 'POST':
        # hardware version
        hv = request.form['hv']
        # firmware version
        fv = request.form['fv']
        # check firmware exist and delete
        delete_firmware(hv, fv)

    return render_template('pages/firmware_delete.html')


@bp.route('/version', methods=["GET", "POST"])
def version():
    if request.method == 'POST':
        return '測試'

    return render_template('pages/firmware_version.html')


@bp.route('/get_FV', methods=["GET", "POST"])
def get_FV():
    FirmwareData_df = pd.read_csv(UPLOAD_FOLDER_firmware + '/FirmwareData.csv')
    FirmwareData_df = FirmwareData_df.where(pd.notnull(FirmwareData_df), None).T.to_dict()  #把空值轉成None
    FirmwareData = [FirmwareData_df[i] for i in FirmwareData_df]
    FirmwareDatalen = len(FirmwareData)
    print(FirmwareData)
    return Response(json.dumps({'datalen': FirmwareDatalen, 'data': FirmwareData}))


@bp.route('/save_FV', methods=["GET", "POST"])
def save_FV():
    if request.method == 'POST':
        table = request.form['table']
        soup = BeautifulSoup(table, 'html.parser', )
        temp = []
        for i in soup.find_all('tr'):
            td = i.find_all('td')
            temp.append((str(td[0])[4:-5],
                         str(td[1])[4:-5],
                         str(td[2])[4:-5],
                         str(td[3])[4:-5],
                         str(td[4])[4:-5],
                         str(td[5])[4:-5],
                         ))
        # 儲存firmware資料
        pd.DataFrame(temp, columns=['datetime', 'hv', 'fv', 'label', 'commit', 'verify']).to_csv(UPLOAD_FOLDER_firmware + '/FirmwareData.csv', index=False)
        return render_template('pages/firmware_version.html')
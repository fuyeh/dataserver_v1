"""Define every forms module"""

from flask_wtf import FlaskForm
from wtforms.validators import DataRequired, EqualTo, Length, Email
from wtforms import TextField, PasswordField, SelectField, IntegerField, SelectMultipleField, widgets


# Set your classes here.
class MultiCheckboxField(SelectMultipleField):
    widget = widgets.ListWidget(prefix_label=False)
    option_widget = widgets.CheckboxInput()


class RegisterForm(FlaskForm):
    username = TextField('Username', validators=[DataRequired(), Length(min=6, max=25)])
    email = TextField('Email', validators=[DataRequired(), Length(min=6, max=40)])
    password = PasswordField('Password', validators=[DataRequired(), Length(min=6, max=40)])
    confirm = PasswordField('Repeat Password', [DataRequired(), EqualTo('password', message='Passwords must match')])


class LoginForm(FlaskForm):
    username = TextField('Username', [DataRequired()])
    email = TextField('Email', validators=[DataRequired(), Length(min=6, max=40)])
    password = PasswordField('Password', [DataRequired()])


class ForgotForm(FlaskForm):
    email = TextField('Email', validators=[DataRequired(), Length(min=6, max=40)])
    newpassword = PasswordField('New Password', [DataRequired()])


class ProfileForm(FlaskForm):
    username = TextField("username", validators=[DataRequired(), Length(min=6, max=25)])
    email = TextField('Email', validators=[DataRequired(), Length(min=6, max=40), Email()])


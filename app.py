"""Defines the app modules."""

import logging
from logging import Formatter, FileHandler

from flask import Flask, render_template, g, session
import werkzeug

import auth
import download
import firmware
import apk
from config import *

app = Flask(__name__)
app.config.from_object('config')
app.secret_key = SECRET_KEY


# db = init_db(app)
app.register_blueprint(auth.bp)
app.register_blueprint(download.bp)
app.register_blueprint(firmware.bp)
app.register_blueprint(apk.bp)


@app.route('/')
def home():
    return render_template('pages/home.html')


@app.route('/index')
def index():
    return render_template('pages/home.html')


@app.route('/about')
def about():
    return render_template('pages/about.html')


@app.before_request
def load_logged_in_user():
    username = session.get("username")
    if username is None:
        g.username = None
    else:
        g.username = username


# Error handlers.
@app.errorhandler(500)
def internal_error(error):
    return render_template('errors/500.html'), 500


@app.errorhandler(404)
def not_found_error(error):
    return render_template('errors/404.html'), 404


@app.errorhandler(werkzeug.exceptions.BadRequest)
def handle_bad_request(e):
    return render_template('errors/400.html'), 400


app.register_error_handler(400, handle_bad_request)

if not app.debug:
    file_handler = FileHandler('error.log')
    file_handler.setFormatter(
        Formatter('%(asctime)s %(levelname)s: %(message)s [in %(pathname)s:%(lineno)d]')
    )
    app.logger.setLevel(logging.INFO)
    file_handler.setLevel(logging.INFO)
    app.logger.addHandler(file_handler)
    app.logger.info('errors')


if __name__ == '__main__':
    app.run(host="127.0.0.1", port=5000)
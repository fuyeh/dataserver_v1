"""Defines the download module."""
from mongoDBManage import *
from config import datapath
from flask import render_template, request, Blueprint, flash, send_file

bp = Blueprint('download', __name__, url_prefix='/download')


@bp.route('/', methods=['GET', 'POST'])
def download_db_data():
    if request.method == 'POST':
        form = request.form.to_dict()
        Stimestamp = datetime.strptime(form['Sdatetime'], "%Y-%m-%dT%H:%M").timestamp()
        Etimestamp = datetime.strptime(form['Edatetime'], "%Y-%m-%dT%H:%M").timestamp()
        keyuserid = emailToUserid(form['email'])
        fileformat = form['fileformat']
        # print(Stimestamp, Etimestamp)
        # 1. 清空BCG、Algo資料夾
        clean(BCGpath)
        clean(Algopath)

        # 2-1. 下載BCG檔案
        if "BCG" in form.keys():
            # 3-1. 判斷是否keyin userid
            if keyuserid:
                # 4. 依照email先創資料夾
                os.mkdir(BCGpath + '/' + form['email'])
                # 5. 再依照userid去找相對應BCG資料
                col_ref, start_timestamp = findBCGByUserid(keyuserid, Stimestamp, Etimestamp)
                result = SaveBCGData(col_ref, start_timestamp, form['email'], fileformat)
                if not result:
                    flash('Please re-enter Email', 'error')
                    return render_template('pages/download.html')

            # 3-2. 搜尋這段時間內使用者id有上傳BCG資料(這邊需要多判斷其他)
            else:
                userids = findUserids(Stimestamp, Etimestamp)
                if userids:
                    for userid in userids:
                        userid = userid['_id']
                        email = useridToEmail(userid)
                        # 4. 依照email先創資料夾
                        os.mkdir(BCGpath + '/' + email)
                        # 5. 再依照userid去找相對應BCG資料
                        col_ref, start_timestamp = findBCGByUserid(userid, Stimestamp, Etimestamp)
                        SaveBCGData(col_ref, start_timestamp, email, fileformat)
                else:
                    flash('Please re-enter Time Interval', 'error')
                    return render_template('pages/download.html')
        # 2-2. 下載Algo檔案
        if "Algo" in form.keys():
            # 3-1. 判斷是否keyin userid
            if keyuserid:
                # 4. 依照email先創資料夾
                os.mkdir(Algopath + '/' + form['email'])
                # 5. 再依照userid去找相對應Algo資料
                col_ref = findAlgoByUserid(keyuserid, Stimestamp, Etimestamp)
                result = SaveAlgoData(col_ref, form['email'], fileformat)
                if not result:
                    flash('Please re-enter Email', 'error')
                    return render_template('pages/download.html')

            # 3-2. 搜尋這段時間內使用者id有上傳BCG資料(這邊需要多判斷其他)
            else:
                userids = findUserids(Stimestamp, Etimestamp)
                if userids:
                    for userid in userids:
                        userid = userid['_id']
                        email = useridToEmail(userid)
                        # 4. 依照email先創資料夾
                        os.mkdir(Algopath + '/' + email)
                        # 5. 再依照userid去找相對應BCG資料
                        col_ref = findAlgoByUserid(userid, Stimestamp, Etimestamp)
                        SaveAlgoData(col_ref, email, fileformat)
                else:
                    flash('Please re-enter Time Interval', 'error')
                    return render_template('pages/download.html')
        # 6. 壓縮/data資料夾
        zipfiledir = zip(datapath)
        # 7. 傳送data.zip
        return send_file(zipfiledir, mimetype='zip', attachment_filename='data.zip', as_attachment=True)

    return render_template('pages/download.html')